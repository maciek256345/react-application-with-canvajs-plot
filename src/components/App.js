import React, {Component} from 'react';

import CSVReader from "react-csv-reader";
import "./styles.css"
import Info from "./IsFileLoaded";
import Plot from "./Plot";
import Buttons from "./Buttons";


const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
};

class App extends Component {

    state = {
        isLoaded: 'no',
        visible1: true,
        visible2: true,
        visible3: true,
        auckland: [],
        waikato: [],
        wellington: [],
        aucklandBtn: 'Hide Auckland',
        waikatoBtn: 'Hide Waikato',
        wellingtonBtn: 'Hide Wellington'
    };

    all = () => {
        this.setState({visible1: true, visible2: true, visible3: true})
    }

    hideAuckland = () => {
        this.setState({visible1: false})
        this.setState({aucklandBtn: 'Show Auckland'})
    }

    showAuckland = () => {
        this.setState({visible1: true})
        this.setState({aucklandBtn: 'Hide Auckland'})
    }

    hideWellington = () => {
        this.setState({visible2: false})
        this.setState({wellingtonBtn: 'Show Wellington'})
    }

    showWellington = () => {
        this.setState({visible2: true})
        this.setState({wellingtonBtn: 'Hide Wellington'})
    }

    hideWaikato = () => {
        this.setState({visible3: false})
        this.setState({waikatoBtn: 'Show Waikato'})
    }

    showWaikato = () => {
        this.setState({visible3: true})
        this.setState({waikatoBtn: 'Hide Waikato'})
    }

    handleForce = (data) => {
        data.map((p) => {
            if (p.region === "Auckland") {
                this.state.auckland.push({x: new Date(p.year, null, null), y: p.data_val})
            } else if (p.region === "Waikato") {
                this.state.waikato.push({x: new Date(p.year, null, null), y: p.data_val})
            } else {
                this.state.wellington.push({x: new Date(p.year, null, null), y: p.data_val})
            }
        })
        this.setState({isLoaded: 'yes'})
    }

    render() {
        if (this.state.isLoaded === "yes") {
            return (
                <div className="App" style={{justifyContent: 'center'}}>
                    <div className="container">
                        <CSVReader
                            cssClass="react-csv-input"
                            label="Select CSV file with CO2 data"
                            onFileLoaded={this.handleForce}
                            parserOptions={papaparseOptions}
                        />
                    </div>
                    <Info loaded={this.state.isLoaded}/>
                    <Buttons clickedHideAuckland={this.hideAuckland} clickedShowAuckland={this.showAuckland}
                             clickedHideWaikato={this.hideWaikato} clickedShowWaikato={this.showWaikato}
                             clickedHideWellington={this.hideWellington} clickedShowWellington={this.showWellington}
                             aucklandBtn={this.state.aucklandBtn} wellingtonBtn={this.state.wellingtonBtn}
                             waikatoBtn={this.state.waikatoBtn}/>

                    <Plot data1={this.state.auckland} data2={this.state.wellington} data3={this.state.waikato}
                          visible1={this.state.visible1} visible2={this.state.visible2} visible3={this.state.visible3}
                    />


                </div>
            );
        } else {
            return (
                <div className="App" style={{justifyContent: 'center'}}>
                    <div className="container">
                        <CSVReader
                            cssClass="react-csv-input"
                            label="Select CSV file with CO2 data"
                            onFileLoaded={this.handleForce}
                            parserOptions={papaparseOptions}
                        />
                    </div>
                    <Info loaded={this.state.isLoaded}/>
                </div>
            )
        }

    }

}

export default App;

