import React from "react";

const Info = props => {
    if (props.loaded === 'no') {
        return (
            <div style={{textAlign: 'center'}}>
                <h2>Waiting for file ... </h2>
            </div>
        )
    } else {
        return (
            <div style={{textAlign: 'center'}}>
                <h2>File loaded successfully !</h2>
            </div>
        )
    }

}
export default Info