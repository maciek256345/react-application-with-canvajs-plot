import React from 'react';
import CanvasJSReact from '../lib/canvasjs.react';
import "./styles.css"

let CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Plot = (props) => {

    const options = {
        zoomEnabled: true,
        backgroundColor: 'snow',
        width: 850,
        height: 450,
        title: {
            text: "CO2 emission, New Zeland"
        },
        axisX: {
            valueFormatString: "YYYY",
            labelAngle: -45,
            title: "Year",
            interlacedColor: "#f8f8f8"
        },
        axisY: {
            title: "Emmision (kilotones)"
        },
        data: [{
            visible: props.visible1,
            name: "Auckland",
            type: "spline",
            showInLegend: true,
            dataPoints: props.data1
        }, {
            visible: props.visible2,
            name: "Wellington",
            type: "line",
            showInLegend: true,
            dataPoints: props.data2
        }, {
            visible: props.visible3,
            name: "Waikato",
            type: "spline",
            showInLegend: true,
            dataPoints: props.data3
        }
        ]

    }

    return (
        <div>
            <CanvasJSChart options={options}/>
        </div>
    )
}
export default Plot;