import React from "react";
import {Button, ButtonGroup, CardContent, Grid, Typography} from "@mui/material";

const Buttons = (props) => {

    var onClickAuckland = null
    var onClickWellington = null
    var onClickWaikato = null

    if (props.aucklandBtn === "Show Auckland") {
        onClickAuckland = props.clickedShowAuckland
    } else {
        onClickAuckland = props.clickedHideAuckland
    }

    if (props.wellingtonBtn === "Show Wellington") {
        onClickWellington = props.clickedShowWellington
    } else {
        onClickWellington = props.clickedHideWellington
    }

    if (props.waikatoBtn === "Show Waikato") {
        onClickWaikato = props.clickedShowWaikato
    } else {
        onClickWaikato = props.clickedHideWaikato
    }


    return (
        <div>
            <Grid container sx={{justifyContent: "center", paddingTop: 5}}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div"
                                style={{fontWeight: 'bold', textAlign: 'center'}}>
                        Choose regions:
                    </Typography>

                    <ButtonGroup disableElevation variant="contained" color="success">

                        <Button onClick={onClickAuckland} color="success">{props.aucklandBtn}</Button>
                        <Button onClick={onClickWellington} color="success">{props.wellingtonBtn}</Button>
                        <Button onClick={onClickWaikato} color="success">{props.waikatoBtn}</Button>

                    </ButtonGroup>
                </CardContent>

            </Grid>
        </div>
    );
}
export default Buttons